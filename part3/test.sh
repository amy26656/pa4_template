#!/bin/bash
set -x
python3 generate_json.py

rm score.txt

cd CACTI
chmod +x evaluate.sh
./evaluate.sh >> ../score.txt
cd ..

mkdir build
cd build
cmake ..
make
./cache_sim --one-line -t ../trace/gcc.trace -c ../config/cache_arc.json >> ../score.txt
./cache_sim --one-line -t ../trace/gzip.trace -c ../config/cache_arc.json >> ../score.txt
./cache_sim --one-line -t ../trace/twolf.trace -c ../config/cache_arc.json >> ../score.txt
./cache_sim --one-line -t ../trace/mcf.trace -c ../config/cache_arc.json >> ../score.txt
./cache_sim --one-line -t ../trace/swim.trace -c ../config/cache_arc.json >> ../score.txt
./cache_sim --one-line -t ../trace/maze.trace -c ../config/cache_arc.json >> ../score.txt
cd ..
chmod +x average.py
python3 average.py


