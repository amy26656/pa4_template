#!/bin/sh

FILENAME=../cache_arc.cfg
COL=33;
cat $FILENAME | while read LINE
do
	#echo $LINE
	#sed -i '/$var/'d file
	sed -i "${COL}c ${LINE}" cache.cfg
	#echo $COL
	COL=$(($COL+1))
done
chmod +x cacti
./cacti -infile cache.cfg > hardware_inf

sed -i '1,57d' hardware_inf
sed -i '9,179d' hardware_inf
chmod +x score.py
SCORE=$(python3 score.py) 
echo "${SCORE}"
