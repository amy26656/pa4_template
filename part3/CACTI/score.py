#!/usr/bin/python
import re
## Open file

with open("hardware_inf", "r") as f:
    para = []
    for l in f:       
        list0 = re.findall('\d+', l)
        if (list0 == []):
            break
        string = list0[0]+'.'+list0[1]
        para.append(float(string))
        if (len(list0) >2):
            string = list0[2]+'.'+list0[3]
            para.append(float(string))            
    score = para[6]*para[7]*100
    print(score)

f.close()