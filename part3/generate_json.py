#!/usr/bin/python
import re
## Open file

with open("cache_arc.cfg", "r") as f:
    para = []
    for l in f:       
        list0 = re.findall('\d+', l)
        if (list0 == []):
            break
        string = list0[0]
        para.append(string)           
    #print(para)
    #score = para[6]*para[7]*100
    #print(score)
f.close()

with open("config/cache_arc.json", "w") as f:
    f.write("{\n\t\"multi-level\": false,\n\t\"content\": [\n\t{\n\t\t\"cache-size\": 64,\n")
    line = "\t\t\"block-size\": "+ para[0]+",\n"
    f.write(line)
    f.write("\t\t\"associativity\": \"set-associative\",\n")
    line = "\t\t\"number-of-way\": "+ para[1]+",\n"
    f.write(line)
    f.write("\t\t\"replacement-policy\": \"lfu\"\n\t}\n\t]\n}\n")
f.close()