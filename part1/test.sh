#!/bin/sh
./cache_sim --one-line -t ../TestData/gcc.trace -c ../TestData/cache1.json
./cache_sim --one-line -t ../TestData/gzip.trace -c ../TestData/cache1.json
./cache_sim --one-line -t ../TestData/mcf.trace -c ../TestData/cache1.json
./cache_sim --one-line -t ../TestData/swim.trace -c ../TestData/cache1.json
./cache_sim --one-line -t ../TestData/twolf.trace -c ../TestData/cache1.json
./cache_sim --one-line -t ../TestData/curl.trace -c ../TestData/cache1.json
./cache_sim --one-line -t ../TestData/ls.trace -c ../TestData/cache1.json
./cache_sim --one-line -t ../TestData/tar.trace -c ../TestData/cache1.json

./cache_sim --one-line -t ../TestData/gcc.trace -c ../TestData/cache2.json
./cache_sim --one-line -t ../TestData/gzip.trace -c ../TestData/cache2.json
./cache_sim --one-line -t ../TestData/mcf.trace -c ../TestData/cache2.json
./cache_sim --one-line -t ../TestData/swim.trace -c ../TestData/cache2.json
./cache_sim --one-line -t ../TestData/twolf.trace -c ../TestData/cache2.json
./cache_sim --one-line -t ../TestData/curl.trace -c ../TestData/cache2.json
./cache_sim --one-line -t ../TestData/ls.trace -c ../TestData/cache2.json
./cache_sim --one-line -t ../TestData/tar.trace -c ../TestData/cache2.json

./cache_sim --one-line -t ../TestData/gcc.trace -c ../TestData/cache3.json
./cache_sim --one-line -t ../TestData/gzip.trace -c ../TestData/cache3.json
./cache_sim --one-line -t ../TestData/mcf.trace -c ../TestData/cache3.json
./cache_sim --one-line -t ../TestData/swim.trace -c ../TestData/cache3.json
./cache_sim --one-line -t ../TestData/twolf.trace -c ../TestData/cache3.json
./cache_sim --one-line -t ../TestData/curl.trace -c ../TestData/cache3.json
./cache_sim --one-line -t ../TestData/ls.trace -c ../TestData/cache3.json
./cache_sim --one-line -t ../TestData/tar.trace -c ../TestData/cache3.json

./cache_sim --one-line -t ../TestData/gcc.trace -c ../TestData/cache4.json
./cache_sim --one-line -t ../TestData/gzip.trace -c ../TestData/cache4.json
./cache_sim --one-line -t ../TestData/mcf.trace -c ../TestData/cache4.json
./cache_sim --one-line -t ../TestData/swim.trace -c ../TestData/cache4.json
./cache_sim --one-line -t ../TestData/twolf.trace -c ../TestData/cache4.json
./cache_sim --one-line -t ../TestData/curl.trace -c ../TestData/cache4.json
./cache_sim --one-line -t ../TestData/ls.trace -c ../TestData/cache4.json
./cache_sim --one-line -t ../TestData/tar.trace -c ../TestData/cache4.json
