# Cache_sim

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/a5a7971e6230473a94f62556a8abe309)](https://www.codacy.com/manual/jerry0715no14/Cache_sim?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=jerryzj/Cache_sim&amp;utm_campaign=Badge_Grade)

Cache Simulator for EE3450 course

## Package requirement

	* clang
	* cmake
	* clang-format

## Design Cache Architecture

In file ``cache_arc.cfg``, you can design your own cache architecture.

``` shell
-block size (bytes) 64
-associativity 4
```

## Run simulation

```shell
make
```

The output will be named as ``score.txt``.

After finish simulation, you can use ``make clean`` to remove the the simulation files.
